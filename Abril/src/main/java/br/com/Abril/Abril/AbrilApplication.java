package br.com.Abril.Abril;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AbrilApplication {

	public static void main(String[] args) {
		SpringApplication.run(AbrilApplication.class, args);
	}

}

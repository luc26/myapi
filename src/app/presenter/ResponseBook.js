class ResponseBook{
    ResponseBookForUser(docs, err){
        const tituloBook = docs.titulo;
        const preco = docs.preco;
        const descricao = docs.descricao;

        const respondeRegisterBook = 
        {
            data : {
                "user" : [
                    {
                        "titulo": "" + tituloBook + "",
                        "preco": "" + preco + "",
                        "descricao": "" + descricao + "",
                        
                    
                            "_links": [
                                {
                                    "href": `/RegisterBook/${tituloBook},${preco},${descricao}`,
                                    "rel": "Cadastrar um novo Livro",
                                    "method": "POST"
                                },
                                {
                                    "href": '/GetBook',
                                    "rel": "Listar todos os livros",
                                    "method": "GET"
                                },
                                {
                                    "href": `/Deletebook/${tituloBook},${preco},${descricao}`,
                                    "rel": "Determinar um determinado livro",
                                    "method": "DELETE"
                                },
                                {
                                    "href":`/ChangeBook/${tituloBook},${preco},${descricao}`,
                                    "rel": "alterar dados de um livro",
                                    "method": "PUT"
                                }
                            ]
                }
             ]
           }
        }
        return respondeRegisterBook;
    }
}

module.exports = new ResponseBook;
class ResponseRegister {
     ResponseRegisterSucessUser(docs, err) {

        if(err){
            console.log('erro ao gerar a hipermidia');
        }
         const docName = docs.nome;
         const docSobrenome =  docs.sobrenome;
         const docRg = docs.rg;
         const docUsername = docs.username;
         const docPassword = docs.password;
        
         const responseForUser = {
                   data : {
                            "user" : [
                                {
                                    "nome": "" + docName + "",
                                    "sobrenome": "" + docSobrenome + "",
                                    "rg": "" + docRg + "",
                                    "username": "" + docUsername + "",
                                    "password": "" + docPassword + "",
                                
                                "_links": [
                                    {
                                        "href": `Login/${docUsername}/${docPassword}`,
                                        "rel": "Login de Usuario",
                                        "method": "GET"
                                    },
                                    {
                                        "href": "UserListAll/",
                                        "rel": "Listar todos so usuários cadastrados no banco",
                                        "method": "GET"
                                    },
                                    {
                                        "href": `alterDataUser/${docRg}`,
                                        "rel": "Alterar informações do usuario",
                                        "method": "PUT"
                                    },
                                    {
                                        "href":`DeleteUserByRg/${docRg}`,
                                        "rel": "Deletar usuários do banco de dados",
                                        "method": "DELETE"
                                    },
                                    {
                                        "href":`ListUserByRg/${docRg}`,
                                        "rel":"Listar usuário por Rg",
                                        "method": "GET"
                                    },
                                    {
                                        "href":`UserRegistration/${docName}/${docSobrenome}/${docRg}/${docUsername}/${docPassword}`,
                                        "rel":"cadastro um novo Usuario",
                                        "method": "POST"
                                    }
                                ]
                            }
                         ]
                    }
                }
        return responseForUser;
    }

}

module.exports = new ResponseRegister;


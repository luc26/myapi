const map = require('./response.json')
const { ResponseRegisterSucessUser } = require('./ResponseRegister')
class Presenter{
    buildRespondeRegisterUser(docs, value,res, err){
        switch(value){
            case 0: 
                    return res.status(500).send(
                        {
                           status: map.erroInserirUsuario.status,
                           message: map.erroInserirUsuario.message,
                           erro :  err
                        })
            break;
            case 1:
                    return res.status(201).send(
                        {
                            status: map.sucessoInserirUsuario.status,
                            message: map.sucessoInserirUsuario.message,
                            data: ResponseRegisterSucessUser(docs)
                        })
            break;
            case 2:
                    return res.status(500).send(
                        {
                            status: map.erroVerificarNaBase.status,
                            message: map.erroVerificarNaBase.message,
                            erro :  err
                        })
            break;
            case 3:
                    return res.status(500).send(
                        {
                            status: map.erroUsuarioExiste.status,
                            message: map.erroUsuarioExiste.message,
                            erro :  docs.nome
                        })
            break;
            case 4:
                    return res.status(500).send(
                        {
                            status: map.erroVerificarRg.status,
                            message: map.erroVerificarRg.message,
                            erro :  err
                        })
            break;
            case 5:
                    return res.status(406).send(
                        {
                            status: map.erroRgExiste.status,
                            message: map.erroRgExiste.message,
                            erro :  err
                        })
            break;
            case 6:
                    return res.status(200).send(
                        {
                            status: map.sucessoUsuarioEncontrado.status,
                            message: map.sucessoUsuarioEncontrado.message,
                            data: docs,
                            _links: ResponseRegisterSucessUser(docs , err)
                        })
            break;
            case 7:
                    return res.status(500).send(
                        {
                            status: map.erroDeletarUsuario.status,
                            message: map.erroDeletarUsuario.message,
                            erro :  err
                        }) 
            break;
            case 8:
                    return res.status(410).send(
                        {
                            status: map.sucessoUsuarioDeletado.status,
                            message: map.sucessoUsuarioDeletado.message,
                            data: ResponseRegisterSucessUser(docs, null)          
                        }) 
            break;
            case 9:
                    return res.status(500).send(
                        {
                            status: map.erroAlterarUsuario.status,
                            message: map.erroAlterarUsuario.message,
                            erro :  err
                        }) 
            break;
            case 10:
                    return res.status(410).send(
                        {
                        status: map.sucessoUsuarioAlterado.status,
                        message: map.sucessoUsuarioAlterado.message,
                        data: ResponseRegisterSucessUser(docs, null)         
                        }) 
            break;
            case 11:
                    return res.status(500).send(
                        {
                        status: map.errorGeneric.status,
                        message: map.errorGeneric.message,
                        erro: err       
                        }) 
             default:
                 return res.status(500).send(
                     {
                     status: map.errorGeneric.status,
                     message: map.errorGeneric.message,
                     erro: err       
                     }) 
        }
    }

    buildRespondeRegisterBook(docs, value, res, err){
       switch(value){
           case 0:
                res.status(400).send({
                    status: map.erroInserirLivroBanco.status,
                    message: map.erroInserirLivroBanco.message,
                    erro: err
                })
            break;
           case 1:
                res.status(201).send({
                    status: map.LivroCadastradoSucesso.status,
                    mensagem: map.LivroCadastradoSucesso.message,
                    data: docs
                })
           break;
           case 2:
                res.status(500).send(
                    {
                            status: map.erroLivroNaoEncontrado.status,
                            message: map.erroLivroNaoEncontrado.message,
                            erro :  err
                    })
           break;
           case 3:
                return res.status(200).send({
                    status: map.LivroEncontrado.status,
                    message: map.LivroEncontrado.message,
                    data: docs
                })
           break;
           case 4:
                res.status(400).send({
                    status: map.erroAlterarLivroBanco.status,
                    message: map.erroAlterarLivroBanco.message,
                    erro: err
               })
           break;
           case 5:
                return res.status(200).send({
                    status: map.LivroDeletadoSucesso.status,
                    mensagem: map.LivroDeletadoSucesso.message,
                    data: docs
                })
           break;
           case 6:
                res.status(400).send({
                    status: map.erroDeletarLivro.status,
                    message: map.erroDeletarLivro.message,
                    erro: err
               })
           break;
           case 7:
                res.status(200).send({
                    status: map.livroAlteradoSucesso.status,
                    mensagem: map.livroAlteradoSucesso.message,
                    data: docEdicao
                })
           break;
       } 
    }

    buildResponseForJasonWebbToken(tokenValid, docs, value, res, err){
        switch(value){
            case 0:
                    res.status(500).send(
                        {
                                status: map.erroLoginNoJwt.status,
                                message: map.erroLoginNoJwt.message,
                                erro :  err
                        });
            break;;
            case 1:;
                    res.status(200).send(
                        {
                                username: docs.username, 
                                auth: true,
                                token: tokenValid
                        });
            break;
            case 2:
                    res.status(200).send(
                        {
                             auth: false,
                             token: null 
                        });
            break;
            case 3:
                    res.status(401).send(
                        {
                                auth:false,
                                message: map.erroTokenEnv.message
                      });
            break;
            case 4:
                    res.status(500).send(
                        {
                            auth: false, 
                            message: map.erroAutToken.message
                        });
            break;
        }
    }
    
}

module.exports = new Presenter;
import React from 'react'
import { BrowserRouter } from 'react-router-dom'


import Menu from './Components/Template/Menu/Menu.jsx'
import Footer from './Components/Template/Footer/Footer.jsx'
import Routes from './Components/Routes.jsx'
import './App.css'

export default props =>
    <BrowserRouter>
        <div className="app">
            <Menu />
            <Routes /> 
            <Footer />
        </div>
    </BrowserRouter>


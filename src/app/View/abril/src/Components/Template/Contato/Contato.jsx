import React from 'react';
import './Contato.css'

export default prompt =>
    <aside className="Contato">
        <div className="myContato">
            <strong>Abril</strong>
            <p>Telefone 1: (11)82123-1222</p>
            <p>Telefone 2: (11)82123-1222</p>
            <p>Telefone 3: (11)82123-1222</p>
            <p>Telefone 4: (11)82123-1222</p>
            <p>Email: AbrilEditora@gmail.com.br</p>
        </div>
    </aside>
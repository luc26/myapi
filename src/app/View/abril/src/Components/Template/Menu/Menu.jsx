import React from 'react'
import './Menu.css'
import { Link } from 'react-router-dom'

export default props =>
    <aside className="menu">
        <nav className="menu-area">
            <Link to="/">
                <i className="textmy"></i>Início
            </Link>
            <Link to="/Users">
                <i className="textmy"></i>Usuário
            </Link>
            <Link to="/Book">
                <i className="textmy"></i>livros
            </Link>
            <Link to="/Contato">
                <i className="textmy"></i>Contato
            </Link>
            <Link to="/QuemSomos">
                <i className="textmy"></i>Quem Somos
            </Link>
        </nav>
    </aside>
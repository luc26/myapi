import  React, { Component } from 'react'
import { Link } from 'react-router-dom'
import axios from 'axios'
import './Book.css'

const urlBook = 'http://localhost:3001/GetBook'
const initialStateBook = {
    book: { 
        _id: '',
        titulo: '',
        preco: '',
        descricao: ''
          },
    list: []
}

export default class Book extends Component {
   
    state = { ...initialStateBook }
   
    componentWillMount(){
        axios.get(urlBook)
                          .then(resp => this.setState({list: resp.data.data}))
                          .catch(err => console.log('Erro ao carregar as informações da página:',err))
                        }
    
    render(){
        return(
            <div className="book-list">
                {this.state.list.map(book => (
                    <article key={book._id}>
                        <strong> {book.titulo} </strong>
                        <p>{book.preco}</p>
                        <p>{book.descricao}</p>
                        <Link to={`/BookDetails/${book._id}`}>Acessar</Link>
                    </article>
                ))}
            </div>
        )
    }
}


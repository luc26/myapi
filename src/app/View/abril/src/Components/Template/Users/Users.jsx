import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import axios from 'axios'
import './Users.css'


const urlUsers = 'http://localhost:3001/UserListAll'
const initialState = {
    user: { 
            _id: '',
            nome: '',
            sobrenome: '',
            rg: '',
            username: '',
            password: '' 
          },
    list: []
}

export default class Users extends Component {

    state = { ...initialState }

    componentWillMount(){
        axios.get(urlUsers)
                          .then(resp => this.setState({ list: resp.data.data }))
                          .catch(err => console.log('Erro ao carregar as informações da página:',err))
    }

    render(){
        return(
            <div className="user-list">
                {this.state.list.map(user => (
                    <article key={user._id}>
                        <strong> {user.nome} </strong>
                        <Link to={`/UsersDetails/${user.rg}`}>Acesssar</Link>
                    </article>
                ))}
            </div>
        )
    }
}
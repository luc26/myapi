import React, { Component } from 'react';
import './UserDetails.css'
import axios from 'axios'


const urlUsers = 'http://localhost:3001/UserListAll'
const initialState = {
    user: { 
        _id: '',
        nome: '',
        sobrenome: '',
        rg: '',
        username: '',
        password: '' 
      },
list: []
}

let myrg = 0;
export default class UserDetails extends Component {
    
    state = {...initialState}

    componentWillMount(){
        const { rg } = this.props.match.params;
        myrg = rg;
        
        axios.get(urlUsers)
                          .then(resp =>  this.getUser(resp.data.data) )
                          .catch(err => console.log('Erro ao carregar as informações da página:',err))
    }

    getUser(data){
        const myusu = data.filter(usu => usu.rg === myrg)
        if(myusu){
            this.setState({list : myusu} )} 
    }
    
    render(){
        return(
            <div className="user-list">
                {this.state.list.map(usuario => (
                    <article key={usuario._id}>
                        <strong>{usuario.nome}</strong>
                        <p>{usuario.sobrenome}</p>
                        <p>{usuario.rg}</p>
                        <p>{usuario.username}</p>
                        <p>{usuario.password}</p>
                    </article>
                ))}
            </div>
        )
    }
}
import React from 'react'
import { Switch, Route, Redirect } from 'react-router'

import Home from './Template/Home/Home.jsx'
import Users from './Template/Users/Users.jsx'
import Book from './Template/Book/Book.jsx'
import QuemSomos from './Template/QuemSomos/QuemSomos.jsx'
import Contato from './Template/Contato/Contato.jsx'
import BookDetails from '/Users/macbookretina/Documents/Code/myapi/src/app/View/abril/src/Components/Template/Book/BookDetails/BookDetails.jsx'
import UsersDetais from '/Users/macbookretina/Documents/Code/myapi/src/app/View/abril/src/Components/Template/Users/UsersDetails/UserDetails.jsx'
export default props =>
    <Switch>
        <Route exact path='/' component={Home}/>
        <Route exact path='/Users' component={Users}/>
        <Route exact path='/Book' component={Book}/>
        <Route exact path='/UsersDetails/:rg' component={UsersDetais}/>
        <Route exact path='/BookDetails/:id' component={BookDetails}/>
        <Route exact path='/QuemSomos' component={QuemSomos}/>
        <Route exact path='/Contato' component={Contato}/>
        <Redirect from='*' to='/'/>
     </Switch> 

